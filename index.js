// Load the expressjs module into our application and saved it in a variable called express
/* 
    Syntax:
        require("package")
*/
const express = require("express");
// localhost port number
const port = 4000;
// App is our server
// Create an application that uses express and stores it as app
const app = express();

// Middleware
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming JSON from our request body
app.use(express.json());


// Mock data
let users = [
    {
        username: "TStart3000",
        email: "starksindustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "loveAndThunder@mail.com",
        password: "iLoveStormBreaker"
    }
];

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
];

// Express has methods to use as routes corresponding to HTTP methods
/* 
    Syntax:
        app.method(<endpoint>, <function for req and res>)
*/

app.get("/", (req, res) => {
    // res.send() actually combines writeHead() and end()
    res.send("Hello from my first expressJS API");
    // res.status(200).send("message")
});

app.get("/greeting", (req, res) => {
    res.send("Hello from Batch203-Clemente");
});

// Retrieval of the users in mock database
app.get("/users", (req, res) => {
    // res.send already stringifies for you
    res.send(users);
});

// Adding of new user
app.post("/users", (req, res) => {
    console.log(req.body); // result: {} empty object
    // Simulate creating a new user document
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    };

    // Adding newUser to the existing array
    users.push(newUser);
    console.log(users);

    // send the updated users array in the client
    res.send(users);
});

// DELETE Method
app.delete("/users", (req, res) => {
    users.pop();
    res.send(users);
});

// PUT Method
// Update user's method
// :index - wildcard
// url: localhost:4000/users/0
app.put("/users/:index", (req, res) => {
    console.log(req.body); // updated password
    // result: {}

    // An object that contains the value of URL params
    console.log(req.params); // refers to the wildcard in the endpoint
    // console.log(typeof req.params); // object
    // result: {index: 0}
    console.log(req.params.index);
    // console.log(typeof req.params.index); // string

    // parseInt the value of the number coming from req.params
    // ["0"] turns into [0]
    let index = parseInt(req.params.index);
    console.log(index);
    console.log(typeof index); //number

    users[index].password = req.body.password;

    // send the updated user
    res.send(users[index]);
});


// [SECTION] ITEMS

app.get("/items", (req, res) => {
    res.send(items);
});

app.post("/items", (req, res) => {
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    };

    items.push(newItem);
    console.log(items);

    res.send(items);
});

app.put("/items/:index", (req, res) => {

    let index = parseInt(req.params.index);

    items[index].price = req.body.price;

    res.send(items[index]);
});

// Port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));
